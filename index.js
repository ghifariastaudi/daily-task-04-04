const fs = require('fs');
const express = require('express');

const temp = fs.readFileSync('./text.txt', 'utf-8');
console.log(temp);

const temp2 = fs.readFileSync('./test.txt', 'utf-8');
console.log(temp2);

//membuat file person.json
const ghip = require('./person.json');
console.log(ghip);

// Nomor 5
const app = express();
app.get('/', (req, res) => {
    res.render('index.ejs');
})

app.get('/person', (req, res) => {
    res.render('person.ejs', {
        data: ghip,
    });
});

app.listen(8000);